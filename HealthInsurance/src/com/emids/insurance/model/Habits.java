package com.emids.insurance.model;
/*
 * Defines habbit of policy holder
 * 
 * 
 * 
 * */

public enum Habits {
	SMOKING(true),
	NO_SMOKING(false),
	ALCOHOLIC(true),
	NONALCOHOLIC(false),
	DAILY_EXERCISE(true),
	NO_DAILY_EXERCISE(false),
	DRUGS(true),
	NO_DRUGS(false);
	
	private Boolean hasHabit;
	Habits(Boolean hasHabit){
		this.hasHabit = hasHabit;
	}
	public Boolean getHasHabit() {
		return hasHabit;
	}
	public void setHasHabit(Boolean hasHabit) {
		this.hasHabit = hasHabit;
	}
	public Boolean getHasGoodHabit() {
		return this.equals(DAILY_EXERCISE);
	}
	public Boolean getHasBadHabit() {
		return (this.equals(NO_DAILY_EXERCISE) || this.equals(DRUGS) || this.equals(ALCOHOLIC) || this.equals(SMOKING));
	}

}
