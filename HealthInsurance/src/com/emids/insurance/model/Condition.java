/**
 * 
 */
package com.emids.insurance.model;

/**
 * @author emidstest05
 * Define health condition
 * this can also be changed to interface to define scale of each condition in class etc.
 */
public enum Condition {
	HYPERTENSION(true),
	BLOOD_PRESSURE(true),
	BLOOD_SUGAR(true),
	OVERWEIGHT(true),
	NO_HYPERTENSION(false),
	NO_BLOOD_PRESSURE(false),
	NO_BLOOD_SUGAR(false),
	NO_OVERWEIGHT(false);

	private Boolean status;


	Condition(Boolean status){
		this.status = status;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	
}
