package com.emids.insurance.model;

import java.util.Date;
import java.util.EnumSet;

/* 
 * class for holding insurance
 * policy holder details
 * 
 * */
public class InsuranceHolderDetails {
	
	private String name;
	private String gender;
	private int age;
	private EnumSet<Condition> healthCondition;
	private EnumSet<Habits> habit;
	private Date dateOfissue;
	private Double premiumAmount;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public Date getDateOfissue() {
		return dateOfissue;
	}
	public void setDateOfissue(Date dateOfissue) {
		this.dateOfissue = dateOfissue;
	}
	public EnumSet<Condition> getHealthCondition() {
		return healthCondition;
	}
	public void setHealthCondition(EnumSet<Condition> healthCondition) {
		this.healthCondition = healthCondition;
	}
	public EnumSet<Habits> getHabit() {
		return habit;
	}
	public void setHabit(EnumSet<Habits> habit) {
		this.habit = habit;
	}
	public Double getPremiumAmount() {
		return premiumAmount;
	}
	public void setPremiumAmount(Double premiumAmount) {
		this.premiumAmount = premiumAmount;
	}
	
	

}
