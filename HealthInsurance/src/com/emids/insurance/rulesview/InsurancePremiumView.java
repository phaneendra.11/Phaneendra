package com.emids.insurance.rulesview;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import org.junit.Test;

import com.emids.insurance.model.Condition;
import com.emids.insurance.model.Habits;
import com.emids.insurance.model.InsuranceHolderDetails;

public class InsurancePremiumView {

	@Test
	public void testHolderDetails() {
		InsuranceHolderDetails holder = new InsuranceHolderDetails();
		holder.setName("Gomes");
		EnumSet<Condition> healthCondition = EnumSet.of(Condition.NO_BLOOD_PRESSURE,Condition.NO_BLOOD_SUGAR,Condition.NO_HYPERTENSION,Condition.OVERWEIGHT);
		holder.setHealthCondition(healthCondition);
		List<String> genValues = Arrays.asList("MALE","FEMALE","OTHERS");
		EnumSet<Habits> habit = EnumSet.of(Habits.ALCOHOLIC,Habits.DAILY_EXERCISE,Habits.NO_DRUGS,Habits.NO_SMOKING);
		holder.setHabit(habit);
		holder.setGender("Email");
		holder.setAge(0);
		assertNotNull(holder.getAge());
		assertNotNull(holder.getGender());
		assertEquals("Gender can't have this value ",true, genValues.contains(holder.getGender().toUpperCase()));
		assertNotSame(0, holder.getAge());
	}

	@Test
	public void testOnAge() {
		InsuranceHolderDetails holder = new InsuranceHolderDetails();
		holder.setName("Gomes");
		holder.setGender("Male");
		holder.setAge(14);
		EnumSet<Condition> healthCondition = EnumSet.of(Condition.NO_BLOOD_PRESSURE,Condition.NO_BLOOD_SUGAR,Condition.NO_HYPERTENSION,Condition.OVERWEIGHT);
		holder.setHealthCondition(healthCondition);
		EnumSet<Habits> habit = EnumSet.of(Habits.ALCOHOLIC,Habits.DAILY_EXERCISE,Habits.NO_DRUGS,Habits.NO_SMOKING);
		holder.setHabit(habit);
		assertNotNull(holder.getAge());
		assertNotNull(holder.getName());
		CalculatePremium premium = new CalculatePremium();
		double premiumAmt = (double)premium.findPremiumOnPreCondition(holder);
		double amount = premiumAmt;
		assertEquals(5145.365,premiumAmt, 0.0D);
		holder.setPremiumAmount(premiumAmt);
		premiumAmt = (double)premium.findPremiumOnAge(holder).doubleValue();
		System.out.println("Health Insurance Premium for "+(holder.getGender().equals("Female")?"Ms.":"Mr.")+holder.getName()+": Rs. "+premiumAmt);
		assertEquals(amount, premiumAmt, 0.0D);
	}
	
	@Test
	public void testWithAllHealthCondition() {
		InsuranceHolderDetails holder = new InsuranceHolderDetails();
		holder.setName("Jordan");
		holder.setGender("Female");
		holder.setAge(45);
//		holder.setDateOfissue(new Date());
		EnumSet<Condition> healthCondition = EnumSet.of(Condition.NO_BLOOD_PRESSURE,Condition.BLOOD_SUGAR,Condition.HYPERTENSION,Condition.OVERWEIGHT);
		holder.setHealthCondition(healthCondition);
		EnumSet<Habits> habit = EnumSet.of(Habits.NONALCOHOLIC,Habits.DAILY_EXERCISE,Habits.NO_DRUGS,Habits.NO_SMOKING);
		holder.setHabit(habit);
		CalculatePremium premium = new CalculatePremium();
		double premiumAmt = (double)premium.findPremiumOnPreCondition(holder);
		holder.setPremiumAmount(premiumAmt);
		premiumAmt = (double)premium.findPremiumOnAge(holder).doubleValue();
		System.out.println("Health Insurance Premium for "+(holder.getGender().equals("Female")?"Ms.":"Mr.")+holder.getName()+": Rs. "+premiumAmt);
		assertEquals(6993.700000000001, premiumAmt, 0.0D);
	}
	@Test
	public void testWithExistingPolicyHolder() {
		InsuranceHolderDetails holder = new InsuranceHolderDetails();
		holder.setName("Gomez");
		holder.setGender("Female");
		holder.setAge(34);
		holder.setPremiumAmount(6000.0);
//		holder.setDateOfissue(new Date(2000,11,12));
		EnumSet<Condition> healthCondition = EnumSet.of(Condition.NO_BLOOD_PRESSURE,Condition.NO_BLOOD_SUGAR,Condition.NO_HYPERTENSION,Condition.OVERWEIGHT);
		holder.setHealthCondition(healthCondition);
		EnumSet<Habits> habit = EnumSet.of(Habits.NONALCOHOLIC,Habits.DAILY_EXERCISE,Habits.NO_DRUGS,Habits.NO_SMOKING);
		holder.setHabit(habit);
		CalculatePremium premium = new CalculatePremium();
		double premiumAmt = (double)premium.findPremiumOnPreCondition(holder);
		holder.setPremiumAmount(premiumAmt);
		premiumAmt = premium.findPremiumOnAge(holder).doubleValue();
		System.out.println("Health Insurance Premium for "+(holder.getGender().equals("Female")?"Ms.":"Mr.")+holder.getName()+": Rs. "+premiumAmt);
		assertEquals(6455.35,premiumAmt, 0.0D);
	}

}
